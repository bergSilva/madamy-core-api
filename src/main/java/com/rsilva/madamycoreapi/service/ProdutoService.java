package com.rsilva.madamycoreapi.service;

import com.rsilva.madamycoreapi.model.dto.ProdutoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProdutoService {
    ProdutoDTO salvar (ProdutoDTO produtoDTO);
    Page<ProdutoDTO> listaTodos(Pageable pageable);
    ProdutoDTO buscarPorId (Long id);
    void deletar(Long id);
}
