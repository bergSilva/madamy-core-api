package com.rsilva.madamycoreapi.service.impl;

import com.rsilva.madamycoreapi.adapter.UsuarioAdapter;
import com.rsilva.madamycoreapi.exceptions.UsuarioNotFoundException;
import com.rsilva.madamycoreapi.model.Usuario;
import com.rsilva.madamycoreapi.model.dto.UsuarioDTO;
import com.rsilva.madamycoreapi.repository.UsuarioRepository;
import com.rsilva.madamycoreapi.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioServiceImpl implements UsuarioService {


    private UsuarioRepository repository;
    private UsuarioAdapter adapter;

    @Autowired
    public UsuarioServiceImpl(UsuarioRepository repository) {
        this.repository = repository;
        this.adapter = new UsuarioAdapter();
    }

    @Override
    public UsuarioDTO salvar(UsuarioDTO usuarioDTO) {
        return this.adapter.toDto(this.repository.save(this.adapter.toEntity(usuarioDTO)));
    }

    @Override
    public Page<UsuarioDTO> listaTodos(Pageable pageable) {
        return this.repository.findAll(pageable).map(this::toDto);
    }

    private UsuarioDTO toDto(Usuario usuario) {
        return this.adapter.toDto(usuario);
    }

    @Override
    public UsuarioDTO buscarPorId(Long id) {
        Optional<Usuario> optional = this.repository.findById(id);
        return adapter.toDto(optional.orElseThrow(UsuarioNotFoundException::new));
    }

    @Override
    public void deletar(Long id) {
        this.repository.deleteById(id);
    }
}
