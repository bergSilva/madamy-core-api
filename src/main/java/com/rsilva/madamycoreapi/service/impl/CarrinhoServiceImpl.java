package com.rsilva.madamycoreapi.service.impl;

import com.rsilva.madamycoreapi.adapter.CarrinhoAdapter;
import com.rsilva.madamycoreapi.exceptions.CarrinhoNotFoundException;
import com.rsilva.madamycoreapi.model.Carrinho;
import com.rsilva.madamycoreapi.model.dto.CarrinhoDTO;
import com.rsilva.madamycoreapi.repository.CarrinhoRepository;
import com.rsilva.madamycoreapi.service.CarrinhoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CarrinhoServiceImpl implements CarrinhoService {

    private CarrinhoRepository repository;
    private CarrinhoAdapter adapter;

    @Autowired
    public CarrinhoServiceImpl (CarrinhoRepository repository){
        this.repository = repository;
        this.adapter = new CarrinhoAdapter();
    }

    @Override
    public CarrinhoDTO salvar(CarrinhoDTO carrinhoDTO) {
        return this.adapter.toDto(this.repository.save(this.adapter.toEntity(carrinhoDTO)));
    }

    @Override
    public Page<CarrinhoDTO>listaTodos(Pageable pageable) {return this.repository.findAll(pageable).map(this::toDto);}

    private CarrinhoDTO toDto(Carrinho carrinho) {
        return this.adapter.toDto(carrinho);
    }

    @Override
    public CarrinhoDTO buscarPorId(Long id) {
        Optional<Carrinho> optional = this.repository.findById(id);
        return adapter.toDto(optional.orElseThrow(CarrinhoNotFoundException::new));
    }

    @Override
    public void deletar(Long id) {
        this.repository.deleteById(id);
    }
}
