package com.rsilva.madamycoreapi.service.impl;

import com.rsilva.madamycoreapi.adapter.ProdutoAdapter;
import com.rsilva.madamycoreapi.exceptions.ProdutoNotFoundException;
import com.rsilva.madamycoreapi.model.Produto;
import com.rsilva.madamycoreapi.model.dto.ProdutoDTO;
import com.rsilva.madamycoreapi.repository.ProdutoRepository;
import com.rsilva.madamycoreapi.service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

    @Service
    public class ProdutoServiceImpl implements ProdutoService {


        private ProdutoRepository repository;
        private ProdutoAdapter adapter;

        @Autowired
        public ProdutoServiceImpl(ProdutoRepository repository) {
            this.repository = repository;
            this.adapter = new ProdutoAdapter();
        }

        @Override
        public ProdutoDTO salvar(ProdutoDTO produtoDTO) {
            return this.adapter.toDto(this.repository.save(this.adapter.toEntity(produtoDTO)));
        }

        @Override
        public Page<ProdutoDTO> listaTodos(Pageable pageable) {
            return this.repository.findAll(pageable).map(this::toDto);
        }

        private ProdutoDTO toDto(Produto produto) {
            return this.adapter.toDto(produto);
        }

        @Override
        public ProdutoDTO buscarPorId(Long id) {
            Optional<Produto> optional = this.repository.findById(id);
            return adapter.toDto(optional.orElseThrow(ProdutoNotFoundException::new));
        }

        @Override
        public void deletar(Long id) {
            this.repository.deleteById(id);
        }
    }
