package com.rsilva.madamycoreapi.service.impl;

import com.rsilva.madamycoreapi.adapter.ItemCarrinhoAdapter;
import com.rsilva.madamycoreapi.exceptions.ItemCarrinhoNotFoundException;
import com.rsilva.madamycoreapi.model.ItemCarrinho;
import com.rsilva.madamycoreapi.model.dto.ItemCarrinhoDTO;
import com.rsilva.madamycoreapi.repository.ItemCarrinhoRepository;
import com.rsilva.madamycoreapi.service.ItemCarrinhoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ItemCarrinhoServiceImpl  implements ItemCarrinhoService {

    private ItemCarrinhoRepository repository;
    private ItemCarrinhoAdapter adapter;

    @Autowired
    public ItemCarrinhoServiceImpl (ItemCarrinhoRepository repository){
        this.repository = repository;
        this.adapter = new ItemCarrinhoAdapter();
    }

    @Override
    public ItemCarrinhoDTO salvar(ItemCarrinhoDTO itemCarrinhoDTO) {
        return this.adapter.toDto(this.repository.save(this.adapter.toEntity(itemCarrinhoDTO)));
    }

    @Override
    public Page<ItemCarrinhoDTO> listaTodos(Pageable pageable) {return this.repository.findAll(pageable).map(this::toDto);}

    private ItemCarrinhoDTO toDto(ItemCarrinho itemCarrinho) {
        return this.adapter.toDto(itemCarrinho);
    }

    @Override
    public ItemCarrinhoDTO buscarPorId(Long id) {
        Optional<ItemCarrinho> optional = this.repository.findById(id);
        return adapter.toDto(optional.orElseThrow(ItemCarrinhoNotFoundException::new));
    }

    @Override
    public void deletar(Long id) {
        this.repository.deleteById(id);
    }
}
