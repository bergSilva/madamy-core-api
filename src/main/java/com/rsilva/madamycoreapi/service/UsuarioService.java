package com.rsilva.madamycoreapi.service;

import com.rsilva.madamycoreapi.model.dto.UsuarioDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UsuarioService {
    UsuarioDTO salvar(UsuarioDTO usuarioDTO);
    Page<UsuarioDTO> listaTodos(Pageable pageable);
    UsuarioDTO buscarPorId(Long id);
    void deletar(Long id);
}
