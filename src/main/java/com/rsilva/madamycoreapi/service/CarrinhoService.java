package com.rsilva.madamycoreapi.service;

import com.rsilva.madamycoreapi.model.dto.CarrinhoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CarrinhoService {
    CarrinhoDTO salvar (CarrinhoDTO carrinhoDTO);
    Page<CarrinhoDTO> listaTodos (Pageable pageable);
    CarrinhoDTO buscarPorId (Long id);
    void deletar(Long id);
}
