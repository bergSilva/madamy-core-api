package com.rsilva.madamycoreapi.service;

import com.rsilva.madamycoreapi.model.dto.ItemCarrinhoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ItemCarrinhoService {
    ItemCarrinhoDTO salvar ( ItemCarrinhoDTO itemCarrinhoDTO);
    Page<ItemCarrinhoDTO> listaTodos (Pageable pageable);
    ItemCarrinhoDTO buscarPorId (Long id);
    void deletar(Long id);

}
