package com.rsilva.madamycoreapi.adapter;

import com.rsilva.madamycoreapi.model.ItemCarrinho;
import com.rsilva.madamycoreapi.model.dto.ItemCarrinhoDTO;

public class ItemCarrinhoAdapter implements GenericAdapter<ItemCarrinho, ItemCarrinhoDTO>{
    @Override
    public ItemCarrinhoDTO toDto(ItemCarrinho itemCarrinho) {
        return ItemCarrinhoDTO.builder()
                .id(itemCarrinho.getId())
                .idCarrinho(itemCarrinho.getIdCarrinho())
                .idProduto(itemCarrinho.getIdProduto())
                .quantidade(itemCarrinho.getQuantidade())
                .flagsStatus(itemCarrinho.getFlagsStatus())
                .build();
    }

    @Override
    public ItemCarrinho toEntity(ItemCarrinhoDTO itemCarrinhoDTO) {
        return ItemCarrinho.builder()
                .id(itemCarrinhoDTO.getId())
                .idCarrinho(itemCarrinhoDTO.getIdCarrinho())
                .idProduto(itemCarrinhoDTO.getIdProduto())
                .quantidade(itemCarrinhoDTO.getQuantidade())
                .flagsStatus(itemCarrinhoDTO.getFlagsStatus())
                .build();
    }
}
