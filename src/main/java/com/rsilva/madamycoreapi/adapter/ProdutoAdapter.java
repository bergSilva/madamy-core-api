package com.rsilva.madamycoreapi.adapter;

import com.rsilva.madamycoreapi.model.Produto;
import com.rsilva.madamycoreapi.model.dto.ProdutoDTO;

public class ProdutoAdapter implements GenericAdapter<Produto, ProdutoDTO> {
    @Override
    public ProdutoDTO toDto (Produto produto){
            return ProdutoDTO.builder()
                    .id(produto.getId())
                    .idProduto(produto.getIdProduto())
                    .Nome(produto.getNome())
                    .quantidade(produto.getQuantidade())
                    .build();
    }
    public Produto toEntity(ProdutoDTO produtoDTO){
            return Produto.builder()
                    .id(produtoDTO.getId())
                    .idProduto(produtoDTO.getIdProduto())
                    .Nome(produtoDTO.getNome())
                    .quantidade(produtoDTO.getQuantidade())
                    .build();

    }
}
