package com.rsilva.madamycoreapi.adapter;

import com.rsilva.madamycoreapi.model.Carrinho;
import com.rsilva.madamycoreapi.model.dto.CarrinhoDTO;

public class CarrinhoAdapter implements GenericAdapter<Carrinho, CarrinhoDTO>{
    @Override
    public CarrinhoDTO toDto(Carrinho carrinho) {
        return CarrinhoDTO.builder()
                .id(carrinho.getId())
                .idUsuario(carrinho.getIdUsuario())
                .nome(carrinho.getNome())
                .dataCompra (carrinho.getDataCompra())
                .build();
    }

    @Override
    public Carrinho toEntity(CarrinhoDTO carrinhoDTO) {
        return Carrinho.builder()
                .id(carrinhoDTO.getId())
                .idUsuario(carrinhoDTO.getIdUsuario())
                .nome(carrinhoDTO.getNome())
                .dataCompra (carrinhoDTO.getDataCompra())
                .build();
    }
}
