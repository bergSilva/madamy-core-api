package com.rsilva.madamycoreapi.adapter;

import com.rsilva.madamycoreapi.model.Usuario;
import com.rsilva.madamycoreapi.model.dto.UsuarioDTO;

public class UsuarioAdapter implements GenericAdapter<Usuario, UsuarioDTO> {
    @Override
    public UsuarioDTO toDto(Usuario usuario) {
        return UsuarioDTO.builder()
                .id(usuario.getId())
                .nome(usuario.getNome())
                .build();
    }

    @Override
    public Usuario toEntity(UsuarioDTO usuarioDTO) {
        return Usuario.builder()
                .id(usuarioDTO.getId())
                .nome(usuarioDTO.getNome())
                .build();
    }
}
