package com.rsilva.madamycoreapi.adapter;

public interface GenericAdapter<T, C> {
    C toDto(T t);

    T toEntity(C c);
}
