package com.rsilva.madamycoreapi.model;

import lombok.*;
import javax.persistence.*;
import java.util.Date;

@Builder
@Getter
@Setter
@Entity
@SequenceGenerator(name = "SEQUENCE", sequenceName = "carrinho_id_seq")
public class Carrinho {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQUENCE")
    private Long id;
    @Column(name = "idUsuario")
    private Long idUsuario;
    @Column(name = "nome")
    private String nome;
    @Column(name = "dataCompra")
    private Date dataCompra;

}

