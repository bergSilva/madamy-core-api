package com.rsilva.madamycoreapi.model;

import lombok.*;
import javax.persistence.*;

@Getter
@Setter
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@SequenceGenerator(name = "SEQUENCE", sequenceName ="ItemCarrinho_id_seq" )
public class ItemCarrinho {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator ="SEQUENCE" )
    private Long id;
    @Column(name = "idCarrinho")
    private Long idCarrinho;
    @Column(name = "idProduto")
    private Long idProduto;
    @Column(name ="quantidade")
    private Long quantidade;
    @Column(name = "flagsStatus")
    private Boolean flagsStatus;
}
