package com.rsilva.madamycoreapi.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class ProdutoDTO {
    private Long id;
    private Long idProduto;
    private String Nome;
    private Long quantidade;
}
