package com.rsilva.madamycoreapi.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class UsuarioDTO {
    private Long id;
    private String nome;
}
