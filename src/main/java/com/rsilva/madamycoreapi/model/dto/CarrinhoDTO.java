package com.rsilva.madamycoreapi.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class CarrinhoDTO {
    private Long id;
    private Long idUsuario;
    private String nome;
    private Date dataCompra;
}
