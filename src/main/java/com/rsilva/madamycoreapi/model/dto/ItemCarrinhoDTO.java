package com.rsilva.madamycoreapi.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class ItemCarrinhoDTO {
    private Long id;
    private Long idCarrinho;
    private Long idProduto;
    private Long quantidade;
    private Boolean flagsStatus;
}
