package com.rsilva.madamycoreapi.model;

import lombok.*;
import javax.persistence.*;
@Getter
@Setter
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@SequenceGenerator(name = "SEQUENCE", sequenceName ="usuario_id_seq" )
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "SEQUENCE")
    private Long id;
    @Column(name = "nome")
    private String nome;
}
