package com.rsilva.madamycoreapi.model;

import lombok.*;
import javax.persistence.*;

@Getter
@Setter
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@SequenceGenerator(name = "SEQUENCE", sequenceName ="Produto_id_seq" )
public class Produto {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "SEQUENCE")
    private Long id;
    @Column(name = "idProduto")
    private Long idProduto;
    @Column(name = "Nome")
    private String Nome;
    @Column(name = "quantidade")
    private Long quantidade;
}
