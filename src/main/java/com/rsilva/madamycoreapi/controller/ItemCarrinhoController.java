package com.rsilva.madamycoreapi.controller;

import com.rsilva.madamycoreapi.model.dto.ItemCarrinhoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/v1/madamy")
public interface ItemCarrinhoController {
    @GetMapping("/itemCarrinho")
    ResponseEntity<Page<ItemCarrinhoDTO>> listarTodosPaginado(Pageable pageable);

    @PostMapping("/itemCarrinho")
    ResponseEntity<ItemCarrinhoDTO>criarItemCarrinho(@RequestBody ItemCarrinhoDTO itemCarrinhoDTO);
}
