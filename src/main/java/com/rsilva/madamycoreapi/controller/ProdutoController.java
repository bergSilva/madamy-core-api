package com.rsilva.madamycoreapi.controller;

import com.rsilva.madamycoreapi.model.dto.ProdutoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/v1/madamy")

public interface ProdutoController {

    @GetMapping("/Produto")
    ResponseEntity<Page<ProdutoDTO>>listarTodosPaginado(Pageable pageable);

    @PostMapping("/Produto")
    ResponseEntity<ProdutoDTO>criarProduto(@RequestBody ProdutoDTO produtoDTO);

}


