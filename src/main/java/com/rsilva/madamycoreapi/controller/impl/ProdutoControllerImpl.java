package com.rsilva.madamycoreapi.controller.impl;

import com.rsilva.madamycoreapi.controller.ProdutoController;
import com.rsilva.madamycoreapi.model.dto.ProdutoDTO;
import com.rsilva.madamycoreapi.service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProdutoControllerImpl implements ProdutoController {

    private ProdutoService service;

    @Autowired
    public ProdutoControllerImpl(ProdutoService service) {
        this.service = service;
    }

    @Override
    public ResponseEntity<Page<ProdutoDTO>> listarTodosPaginado(Pageable pageable) {
        return ResponseEntity.ok(service.listaTodos(pageable));
    }

    @Override
    public ResponseEntity<ProdutoDTO> criarProduto(ProdutoDTO produtoDTO) {
        return ResponseEntity.ok(this.service.salvar(produtoDTO));
    }

}
