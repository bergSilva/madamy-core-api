package com.rsilva.madamycoreapi.controller.impl;

import com.rsilva.madamycoreapi.controller.UsuarioController;
import com.rsilva.madamycoreapi.model.dto.UsuarioDTO;
import com.rsilva.madamycoreapi.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioControllerImpl implements UsuarioController {

    private UsuarioService service;

    @Autowired
    public UsuarioControllerImpl(UsuarioService service) {
        this.service = service;
    }

    @Override
    public ResponseEntity<Page<UsuarioDTO>> listarTodosPaginado(Pageable pageable) {
        return ResponseEntity.ok(service.listaTodos(pageable));
    }

    @Override
    public ResponseEntity<UsuarioDTO> criarUsuario(UsuarioDTO usuarioDTO) {
        return ResponseEntity.ok(this.service.salvar(usuarioDTO));
    }
}
