package com.rsilva.madamycoreapi.controller.impl;

import com.rsilva.madamycoreapi.controller.CarrinhoController;
import com.rsilva.madamycoreapi.model.dto.CarrinhoDTO;
import com.rsilva.madamycoreapi.service.CarrinhoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarrinhoControllerImpl implements CarrinhoController {

    private CarrinhoService service;

    @Autowired
    public CarrinhoControllerImpl (CarrinhoService service) {
        this.service = service;
    }

    @Override
    public ResponseEntity<Page<CarrinhoDTO>> listarTodosPaginado(Pageable pageable) {
        return ResponseEntity.ok(service.listaTodos(pageable));
    }

    @Override
    public ResponseEntity<CarrinhoDTO> criarCarrinho(CarrinhoDTO carrinhoDTO) {
        return ResponseEntity.ok(this.service.salvar(carrinhoDTO));
    }

}
