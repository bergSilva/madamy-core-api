package com.rsilva.madamycoreapi.controller.impl;

import com.rsilva.madamycoreapi.controller.ItemCarrinhoController;
import com.rsilva.madamycoreapi.model.dto.ItemCarrinhoDTO;;
import com.rsilva.madamycoreapi.service.ItemCarrinhoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ItemCarrinhoControllerImpl implements ItemCarrinhoController {

    private ItemCarrinhoService service;

    @Autowired
    public ItemCarrinhoControllerImpl (ItemCarrinhoService service) {
        this.service = service;
    }

    @Override
    public ResponseEntity<Page<ItemCarrinhoDTO>> listarTodosPaginado(Pageable pageable) {
        return ResponseEntity.ok(service.listaTodos(pageable));
    }

    @Override
    public ResponseEntity<ItemCarrinhoDTO> criarItemCarrinho(ItemCarrinhoDTO itemCarrinhoDTO) {
        return ResponseEntity.ok(this.service.salvar(itemCarrinhoDTO));
    }

}
