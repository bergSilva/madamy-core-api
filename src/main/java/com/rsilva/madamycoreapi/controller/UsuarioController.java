package com.rsilva.madamycoreapi.controller;

import com.rsilva.madamycoreapi.model.dto.UsuarioDTO;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/v1/madamy")
public interface UsuarioController {

    @GetMapping("/usuario")
    ResponseEntity<Page<UsuarioDTO>> listarTodosPaginado(Pageable pageable);

    @PostMapping("/usuario")
    ResponseEntity<UsuarioDTO> criarUsuario(@RequestBody UsuarioDTO usuarioDTO);

}
