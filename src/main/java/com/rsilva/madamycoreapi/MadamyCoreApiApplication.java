package com.rsilva.madamycoreapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MadamyCoreApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MadamyCoreApiApplication.class, args);
	}

}
